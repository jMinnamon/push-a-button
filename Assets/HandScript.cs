﻿using UnityEngine;
using System.Collections;

public class HandScript : MonoBehaviour {

	public float moveSpeed = 5.0f;

	// Use this for initialization
	void Update () {
		if(Input.GetKeyDown("space")) {
			//PushButton();
			Debug.Log("boink!");
			this.GetComponent<Animator>().SetTrigger("Push");
		}
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"),Input.GetAxisRaw("Vertical"),0);
		transform.position += moveDirection*moveSpeed*Time.deltaTime;
	}

	void PushButton(){
		Debug.Log ("boink");
		
	}
	
}
