using UnityEngine;
using System.Collections;

public class ShootForward : MonoBehaviour {
	
	public GameObject bullet;
	public float bulletSpeed = 10.0f;
	public float shootDelay = 0.2f;
	
	// Use this for initialization
	void Start () {
		Invoke("Shoot", shootDelay);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void Shoot()
	{
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		if(player)
		{
			
			GameObject bulletInstance = Instantiate(bullet,transform.position,Quaternion.LookRotation(transform.forward)) as GameObject;
			bulletInstance.rigidbody.AddForce(transform.forward*bulletSpeed,ForceMode.VelocityChange);
			Invoke("Shoot", shootDelay);
	
		}
	}
}
