using UnityEngine;
using System.Collections;

public class LoadLevelOnTriggerEnter : MonoBehaviour {

	public string playerTag = "Player";
	public string levelName;
	
	void OnTriggerEnter(Collider col)
	{
		if(col.tag == playerTag)
		{
			Application.LoadLevel(levelName);
		}
	}
}
