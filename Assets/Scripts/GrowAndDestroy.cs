using UnityEngine;
using System.Collections;

public class GrowAndDestroy : MonoBehaviour {
	
	public float initialRadius = 1.0f;
	public float finalRadius = 5.0f;
	public float growTime = 1.0f;
	
	
	// Use this for initialization
	void Start () 
	{
		StartCoroutine(GrowAndKill());
	}
	
	IEnumerator GrowAndKill()
	{
		float t = 0f;
		
		Vector3 initialSize = new Vector3(initialRadius,initialRadius,initialRadius);
		Vector3 finalSize = new Vector3(finalRadius,finalRadius,finalRadius);
		
		while(t<growTime)
		{
			transform.localScale = Vector3.Lerp(initialSize,finalSize,t/growTime);
			t += Time.deltaTime;
			yield return null;
		}
		
		Destroy(gameObject);
	}
	
	
	// Update is called once per frame
	void Update () {
	
	}
}
