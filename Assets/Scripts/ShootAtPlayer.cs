using UnityEngine;
using System.Collections;

public class ShootAtPlayer : MonoBehaviour 
{
	public GameObject bullet;
	public float bulletSpeed = 10.0f;
	public float shootDelay = 0.2f;
	

	void Start()
	{
		Invoke("Shoot", shootDelay);
	}
		
		
	void Shoot()
	{
		Vector3 playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
		Vector3 shootDirection = (playerPosition - transform.position).normalized;
		GameObject bulletInstance = Instantiate(bullet,transform.position,Quaternion.LookRotation(shootDirection)) as GameObject;
		bulletInstance.rigidbody.AddForce(shootDirection*bulletSpeed,ForceMode.VelocityChange);
		Invoke("Shoot", shootDelay);
		gameObject.transform.rotation = Quaternion.LookRotation(shootDirection);
	
	}
	
	void Update()
	{
		Vector3 playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
		Vector3 shootDirection = (playerPosition - transform.position).normalized;
		gameObject.transform.rotation = Quaternion.LookRotation(shootDirection);
	}
		
}
