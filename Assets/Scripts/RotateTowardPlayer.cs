using UnityEngine;
using System.Collections;

public class RotateTowardPlayer : MonoBehaviour {
	public float rotateSpeed = 1.0f;
	
		// Update is called once per frame
	void Update () 
	{
		
		GameObject player = GameObject.FindGameObjectWithTag("Player");
					
		
		if(player)
		{
			Vector3 playerPosition = player.transform.position;
			Vector3 aimVector = playerPosition - transform.position;
			
			//transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, aimVector, rotateSpeed*Time.deltaTime, rotateSpeed),transform.up);
			transform.rotation = Quaternion.RotateTowards(Quaternion.LookRotation(transform.forward,transform.up),Quaternion.LookRotation(aimVector,transform.up), Mathf.Rad2Deg*rotateSpeed*Time.deltaTime);
		}
	}
}
