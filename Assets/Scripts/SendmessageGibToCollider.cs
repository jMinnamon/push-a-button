using UnityEngine;
using System.Collections;

public class SendmessageGibToCollider : MonoBehaviour {

	void OnTriggerEnter(Collider col)
	{
		col.SendMessage("Gib",SendMessageOptions.DontRequireReceiver);
	}
}
