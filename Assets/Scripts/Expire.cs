using UnityEngine;
using System.Collections;

public class Expire : MonoBehaviour
{
	public float itemLife = 10.0f;

	// Use this for initialization
	void Start ()
	{
		Invoke("Expiration",itemLife);
	}
	
	
	void Expiration ()
	{
		Destroy(gameObject);
	}
}

