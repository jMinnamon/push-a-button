using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {
	
	public float speed = 3.0f;
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		if(player)
		{
		Vector3 moveDirection = (player.transform.position - transform.position).normalized;
		transform.position += moveDirection*speed*Time.deltaTime;
		}
		
	}
}
