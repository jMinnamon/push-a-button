using UnityEngine;
using System.Collections;

public class KillManager : MonoBehaviour {
	
	//Assume this will only exist on one object
	public static int killCount = 0;
	public static float powerupTimer = 0.0f;
	public static GameObject player;
	public static GameObject newWeapon;
	public static int killTarget;
	public static bool victory = false;
	
	public int killsToWin = 10;
	
	void Start()
	{
		killCount = 0;
		killTarget = killsToWin;
	}
	
	void Update()
	{
		if(killCount>=killTarget)
		{
			victory = true;
		}
	}
	
	void Powerup()
	{
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		DualStickShooter shootScript = player.GetComponent<DualStickShooter>();
		shootScript.bullet = newWeapon;
		Invoke("PowerupTimeout", powerupTimer);
		Debug.Log("Pppowerd up!");
	}
	void PowerupTimeout()
	{
		Debug.Log("Pwiernog dwon");
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		if(player)
		{
			DualStickShooter shootScript = player.GetComponent<DualStickShooter>();
			shootScript.bullet = shootScript.originalBullet;
			Debug.Log("Unpowered");
		}
	}
}
