using UnityEngine;
using System.Collections;

public class WeaponPowerup : MonoBehaviour {

	public GameObject newWeapon;
	private GameObject manager; 
	
	public float powerupTime = 10.0f;
	
	void Start()
	{
		manager = GameObject.FindGameObjectWithTag("GameController");
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.tag == "Player")
		{
			
			//Destroy existing powerups
			foreach(Transform child in collision.transform)
			{
				if(child.tag == "Powerup")
				{
					Destroy(child.gameObject);
				}
			}
			
			
			//change player weapon
						
			KillManager.newWeapon = newWeapon;
			//KillManager.player = collision.gameObject;
			KillManager.powerupTimer += powerupTime;
			manager.SendMessage("Powerup");
			//DualStickShooter shootScript = collision.gameObject.GetComponent<DualStickShooter>();
			//shootScript.bullet = newWeapon;
			//Invoke("PowerupTimeout", powerupTime);
			
			Destroy(gameObject);
			//transform.parent = collision.transform;
			//Destroy(rigidbody);
			//Destroy(renderer);
			//Destroy(collider);
		}
	}
	
//	void PowerupTimeout()
//	{
//		GameObject player = GameObject.FindGameObjectWithTag("Player");
//		DualStickShooter shootScript = player.GetComponent<DualStickShooter>();
//		shootScript.bullet = shootScript.originalBullet;
//		Destroy(gameObject,2.0f);
//	}
}
