using UnityEngine;
using System.Collections;

public class ExplodeOnColision : MonoBehaviour {
	public GameObject explosionDebris;
	public GameObject explosionObject;
	
	public int amount = 10;
	public float spawnRadius = 0.5f;
	public float explosionRadius = 3.0f;
	public float explosionForce = 500f;
	
	public string[] validTags;
	
	
	void OnCollisionEnter(Collision collision)
	{
		foreach(string tag in validTags)
		{
			if(collision.gameObject.tag == tag)
			{
				SpawnExplosion();
			}

		}
		if(validTags.Length == 0)
		{
			SpawnExplosion();
		}
	}
	
	void SpawnExplosion()
	{
		for(int i = 0; i < amount;  i ++)
				{
					Vector3 spawnPosition = transform.position + Random.onUnitSphere*spawnRadius;
					GameObject gibInstance = Instantiate(explosionDebris,spawnPosition,explosionDebris.transform.rotation) as GameObject;
					gibInstance.rigidbody.AddExplosionForce(explosionForce,transform.position,explosionRadius);
					Instantiate(explosionObject,transform.position,explosionObject.transform.rotation);
					Destroy(gameObject);
				
				
				}
	}
}
