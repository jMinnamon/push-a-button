using UnityEngine;
using System.Collections;

public class DestroyOnVictory : MonoBehaviour {
	
	
	public int numberOfKills = 10;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (KillManager.victory == true)
		{
			Destroy(gameObject);
		}
	}
		
}
