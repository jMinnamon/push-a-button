using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	
	public GameObject enemyType;
	public float spawnRadius = 10.0f;
	public float spawnDelay = 1.0f;
	public bool spawn = true;
	public bool spawnInsideCircle = true;
	private Vector3 spawnPosition;
	
	// Use this for initialization
	void Start () 
	{
		if(spawn) Invoke ("SpawnEnemy", spawnDelay);
	}
	
	void SpawnEnemy()
	{
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		
		if(spawn && player)
		{
			if(spawnInsideCircle)
			{
				Vector3 spawnPosition = transform.position + ((Vector3)Random.insideUnitCircle).normalized*spawnRadius;
				Instantiate(enemyType, spawnPosition, enemyType.transform.rotation);
				Invoke ("SpawnEnemy", spawnDelay);
			}
			else
			{
				Vector3 spawnPosition = transform.position + ((Vector3)Random.insideUnitCircle)*spawnRadius;
				Instantiate(enemyType, spawnPosition, enemyType.transform.rotation);
				Invoke ("SpawnEnemy", spawnDelay);
			}

		}
	}
	
	
	void OnDrawGizmosSelected()
	{
		Gizmos.DrawWireSphere(transform.position, spawnRadius);
	}
}
