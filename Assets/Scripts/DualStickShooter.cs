using UnityEngine;
using System.Collections;

public class DualStickShooter : MonoBehaviour 
{
	public float moveSpeed = 5.0f;
	
	
	
	public GameObject bullet;
	public float bulletSpeed = 10.0f;
	public float shootDelay = 0.1f;
	
	private bool canShoot = true;
	[HideInInspector]
	public GameObject originalBullet;
	
	// Use this for initialization
	void Start () 
	{
		originalBullet = bullet;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		
		Vector3 moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"),Input.GetAxisRaw("Vertical"),0);
		transform.position += moveDirection*moveSpeed*Time.deltaTime;
		
		
		
		/*if((Input.GetAxisRaw("FireHorizontal") != 0.0f || Input.GetAxisRaw("FireVertical") != 0.0f) && canShoot)
		{
			Vector3 shootDirection = new Vector3(Input.GetAxisRaw("FireHorizontal"),Input.GetAxisRaw("FireVertical"),0).normalized;
			GameObject bulletInstance = Instantiate(bullet,transform.position,Quaternion.LookRotation(shootDirection)) as GameObject;
			bulletInstance.rigidbody.AddForce(shootDirection*bulletSpeed,ForceMode.VelocityChange);
		canShoot = false;
		Invoke("ShootDelay", shootDelay);
		}
		*/
	}
	
	void OnCollisionEnter(){rigidbody.velocity = Vector3.zero;}
	void OnCollisionExit(){rigidbody.velocity = Vector3.zero;}
	
	void ShootDelay()
		{
			canShoot = true;
		}
		
		
}
